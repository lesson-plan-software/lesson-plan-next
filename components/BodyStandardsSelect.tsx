import React from "react";

import { CheckboxChip } from "./formInputs";

const BodyStandardsSelect = values => {
  const handleCheck = item => {
    const ids = values;
    const newIds = ids?.some(el => el.standard.standardId === item.standardId);

    if (newIds) {
      return ids?.filter(el => el.standard.standardId !== item.standardId);
    }

    return [...(ids ?? []), { standard: item, dok: "" }];
  };

  return (
    <div>
      <CheckboxChip />
    </div>
  );
};

export default BodyStandardsSelect;
