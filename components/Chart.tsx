import { Bar, CartesianGrid, ComposedChart, Legend, Line, ResponsiveContainer, XAxis, YAxis } from "recharts";

const data = [
  {
    name: "Social Studies",
    minutes: 590,
    activities: 800,
    amt: 1400
  },
  {
    name: "Math",
    minutes: 868,
    activities: 967,
    amt: 1506
  },
  {
    name: "PE",
    minutes: 1397,
    activities: 1098,
    amt: 989
  },
  {
    name: "Art",
    minutes: 1480,
    activities: 1200,
    amt: 1228
  },
  {
    name: "Page E",
    minutes: 1520,
    activities: 1108,
    amt: 1100
  },
  {
    name: "Page F",
    minutes: 1400,
    activities: 680,
    amt: 1700
  }
];

const Chart = () => (
  <ResponsiveContainer width="100%" height="100%">
    <ComposedChart
      width={500}
      height={400}
      data={data}
      margin={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 0
      }}
    >
      <CartesianGrid stroke="#f5f5f5" />
      <XAxis dataKey="name" scale="band" />
      <YAxis />
      {/* <Tooltip /> */}
      <Legend />
      <Bar dataKey="minutes" barSize={20} fill="#002a5e" />
      <Line type="monotone" dataKey="activities" stroke="#ff7300" />
    </ComposedChart>
  </ResponsiveContainer>
);

export default Chart;
