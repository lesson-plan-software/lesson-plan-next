import { Col, Grid } from "@mantine/core";
import React, { useState } from "react";
import { useQuery } from "react-query";

import LessonCalendar from "../components/LessonCalendar";
import UnitPlanCard from "../components/UnitPlanCard";

const fetcher = (date: Date) =>
  fetch("/api/lessonPlans", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(date)
  }).then(res => res.json());

const LessonView = () => {
  const [date, setDate] = useState(new Date(new Date().setHours(0, 0, 0, 0)));
  const { isLoading, data, isSuccess } = useQuery(["lessonPlansDate", date], () => fetcher(date), {
    refetchInterval: false,
    refetchIntervalInBackground: false,
    refetchOnMount: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false
  });

  return (
    <>
      <LessonCalendar callback={setDate} value={date} />
      {isSuccess && (
        <Grid>
          {data.map((lesson, i) => (
            <Col span={12} key={i}>
              <UnitPlanCard name={lesson.planName} grade={lesson.period[0].grade} date={lesson.date} />
            </Col>
          ))}
        </Grid>
      )}
    </>
  );
};

export default LessonView;
