import { Group, SelectItemProps, Text } from "@mantine/core";
import { PeriodDropdown } from "fakeData/periods";
import React, { useState } from "react";
import { Control, useFormContext } from "react-hook-form";

import { MultiSelect } from "./formInputs";

interface SelectItemPropsExt extends SelectItemProps {
  description: string;
}

const SelectItem = React.forwardRef<HTMLDivElement, SelectItemProps>(
  ({ label, description, ...others }: SelectItemPropsExt, ref) => (
    <div ref={ref} {...others}>
      <Group noWrap>
        <div>
          <Text>{label}</Text>
          <Text size="xs" color="dimmed">
            {description}
          </Text>
        </div>
      </Group>
    </div>
  )
);

interface Dropdown {
  value: string;
  label: string;
  description: string;
}

interface PeriodSelectProps {
  control: Control;
  periodOptions: Dropdown[];
}

const PeriodSelect = ({ control, periodOptions }: PeriodSelectProps) => {
  const { setValue, getValues } = useFormContext();

  return (
    <div>
      <MultiSelect
        data={periodOptions}
        name="period"
        label="Period(s)"
        placeholder="Period"
        control={control}
        nothingFound="No options"
        itemComponent={SelectItem}
        onChange={value => {
          const conv = value.map(p => {
            const found = PeriodDropdown.find(o => o.value === p);
            const ob = found?.obj;
            return ob;
          });
          setValue("period", conv);
        }}
        value={getValues("period").map(e => JSON.stringify(e))}
      />
    </div>
  );
};

export default PeriodSelect;
