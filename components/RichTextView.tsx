// RichText.tsx in your components folder
import dynamic from "next/dynamic";

const RichText = dynamic(() => import("@mantine/rte"), {
  // Disable during server side rendering
  ssr: false,

  // Render anything as fallback on server, e.g. loader or html content without editor
  loading: () => null
});

interface Props {
  value: string;
}

const RichTextView = (props: Props) => <RichText readOnly onChange={() => {}} {...props} />;

export default RichTextView;
