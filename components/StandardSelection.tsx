import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useFormContext } from "react-hook-form";

import standards from "../testStandards.json";
import CollapseContentArea from "./CollapseContentArea";
import { CheckboxGroup } from "./formInputs";

const makeCheckboxItem = (subject, area) => {
  const checkboxItem = subject[area].map(item => ({
    id: item.standardId,
    label: item.standard,
    value: {
      band_course: item.band_course,
      content_area: item.content_area,
      grade: item.grade,
      standard: item.standard,
      standardId: item.standardId,
      subject: item.subject
    }
  }));

  return checkboxItem;
};

const findSelected = (area: string, chosen: any[]) => {
  const found = chosen.find(el => el.content_area === area);

  if (found) {
    return true;
  }
  return false;
};

const StandardSelection = () => {
  const { control, watch } = useFormContext();
  const period = watch("period");
  const subjectsWatch = watch("subjects");
  const standardsChosen = watch("standardsChosen");
  const [myContentArea, setMyContentArea] = useState([]);

  useEffect(() => {
    if (period.length > 0) {
      const periodSubjects = period.map(p => `${p.grade}_${p.subject.toUpperCase()}`);

      const additionalSubjects = subjectsWatch.map(subject => `${period[0].grade}_${subject.toUpperCase()}`);

      const gradeSubject = [...periodSubjects, ...(additionalSubjects || [])];

      const applicableStandards = gradeSubject.map(id => standards[id]);

      const grouped = applicableStandards.map(standard => _.groupBy(standard, "content_area"));

      setMyContentArea(grouped);
    } else {
      setMyContentArea([]);
    }
  }, [period, subjectsWatch]);

  return (
    <div>
      {myContentArea.map(subject =>
        Object.keys(subject).map(area => {
          const checkboxItem = makeCheckboxItem(subject, area);
          const selected = findSelected(area, standardsChosen);
          return (
            <CollapseContentArea selected={selected} key={area} title={area}>
              <CheckboxGroup control={control} items={checkboxItem} name="standardsChosen" />
            </CollapseContentArea>
          );
        })
      )}
    </div>
  );
};

export default StandardSelection;
