const SummarySection = ({ children }) => <div style={{ margin: "15px" }}>{children}</div>;

export default SummarySection;
