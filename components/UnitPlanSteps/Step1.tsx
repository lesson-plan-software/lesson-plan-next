import PeriodSelect from "@components/PeriodSelect";
import { Col, Grid, Space, Text } from "@mantine/core";
import dayjs from "dayjs";
import React, { useEffect, useState } from "react";
import { useFormContext } from "react-hook-form";
import { useQuery } from "react-query";
import { numberOrdinal } from "utils/helpers";

import careers from "../../constants/careers";
import { PeriodDropdown } from "../../fakeData/periods";
import { DatePicker, MultiSelect, TextInput } from "../formInputs";
import StandardSelection from "../StandardSelection";

const fetcher = periods => {
  const parsed = periods.map(p => `${p.grade}_${p.subject.toUpperCase()}`);

  return fetch("/api/standards?find=K_MATH", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(parsed)
  }).then(res => res.json());
};

const Step1 = () => {
  const { control, watch } = useFormContext();
  const startDate = watch("date.start");
  const periods = watch("period");
  const [myPeriods, setMyPeriods] = useState(PeriodDropdown);

  const { data, refetch } = useQuery(["standards"], () => fetcher(periods), {
    enabled: false
  });

  useEffect(() => {
    if (periods.length === 1) {
      setMyPeriods(PeriodDropdown.filter(p => p.grade === periods[0].grade));
    }

    refetch().catch((err: any) => console.log(err));

    if (periods.length === 0) {
      setMyPeriods(PeriodDropdown);
    }
  }, [periods]);

  return (
    <div>
      <Grid>
        <Col span={4}>
          <PeriodSelect control={control} periodOptions={myPeriods} />
        </Col>
        <Col span={4}>
          <TextInput placeholder="My unit plan ..." label="Unit Plan Name" control={control} name="planName" />
        </Col>
        <Col span={2}>
          <DatePicker
            control={control}
            name="date.start"
            label="Start Date"
            placeholder={dayjs().format("MM-DD-YYYY")}
            minDate={dayjs(new Date()).startOf("d").toDate()}
            inputFormat="MM/DD/YYYY"
            labelFormat="MM/YYYY"
          />
        </Col>
        <Col span={2}>
          <DatePicker
            control={control}
            name="date.end"
            label="End Date"
            placeholder={dayjs().format("MM-DD-YYYY")}
            minDate={startDate}
            inputFormat="MM/DD/YYYY"
            labelFormat="MM/YYYY"
          />
        </Col>
      </Grid>
      <Space h="xl" />
      {periods.length > 0 && (
        <Text>
          You are planning for{" "}
          {periods.map((p, index) => {
            const last = periods.length > 1 && index < periods.length - 1;
            return (
              <span key={p.period}>
                {`${numberOrdinal(p.grade)} ${p.subject}`}
                {last ? " & " : null}
              </span>
            );
          })}
        </Text>
      )}
      <Space h="xl" />
      <Grid>
        <Col span={6}>
          {/* <MultiSelect
            data={subjects}
            name="subjects"
            placeholder="Additional Subjects to Integrate"
            control={control}
          /> */}
        </Col>
      </Grid>
      <Space h="xl" />
      <StandardSelection />
      <Space h="xl" />
      <Grid>
        <Col span={4}>
          <MultiSelect
            data={careers}
            name="careers"
            label="Career Pathways"
            placeholder="Careers ..."
            control={control}
          />
        </Col>
      </Grid>
      <pre style={{ overflow: "auto" }}>{JSON.stringify(watch(), null, 2)}</pre>
    </div>
  );
};

export default Step1;
