import RichTextView from "@components/RichTextView";
import { Button, Container, Divider, Group, List, Select, Text } from "@mantine/core";
import dayjs from "dayjs";
import Image from "next/image";
import React, { useState } from "react";
import { useFormContext } from "react-hook-form";
import { getDatesBetweenDates } from "utils/helpers";
import { Printable, usePrint } from "utils/printAndSave";

import Table from "../../components/StyledTable";
import Section from "../../components/SummarySection";

const makeStandardsTable = values => {
  const standardsData = values.body.map(b => ({
    time: b.time,
    standards: b.standards.map(s => ({
      standard: s.standard.standard,
      dok: s.dok
    }))
  }));

  const reducedStandards = standardsData.reduce((obj, curr) => {
    const standards = curr.standards.map(s => {
      const found = obj.findIndex(el => el.standard === s.standard && el.dok === s.dok);
      if (found !== -1) {
        obj[found] = { ...obj[found], time: obj[found].time + curr.time };
        return {};
      }
      return {
        standard: s.standard,
        time: curr.time,
        dok: s.dok
      };
    });

    const filtered = standards.filter(s => Object.keys(s).length > 0);

    return [...obj, ...filtered];
  }, []);

  return reducedStandards;
};

const LessonSummary = () => {
  const { componentRef, handlePrint } = usePrint();
  const columns = React.useMemo(
    () => [
      {
        Header: "Standard",
        accessor: "standard"
      },
      {
        Header: "Time Spent",
        accessor: "time"
      },
      {
        Header: "DOK Level",
        accessor: "dok"
      }
    ],
    []
  );

  const { getValues } = useFormContext();
  const period = getValues("period");
  const gradeSubject = { grade: period[0].grade, subject: period[0].subject };
  const subject = getValues("subjects");
  const [currentDay, setCurrentDay] = useState("0");
  const values = getValues(`lessons.${currentDay}`) || {
    opening: {
      time: 1,
      strategy: [],
      details: ""
    },
    body: [
      {
        time: 1,
        workType: "",
        materials: "",
        strategy: [],
        standards: []
      }
    ],
    closing: {
      time: 1,
      strategy: [],
      details: ""
    }
  };

  const data = React.useMemo(() => makeStandardsTable(values), [currentDay]);

  const dates = getDatesBetweenDates(getValues("date.start"), getValues("date.end"));
  const dropdown = dates.map((date, index) => ({
    label: dayjs(date).toString(),
    value: index.toString()
  }));

  console.log(getValues());
  return (
    <Container>
      <Group position="right">
        <Select data={dropdown} placeholder="Select Date" value={currentDay} onChange={setCurrentDay} />
        <Button style={{ marginRight: "8px" }} onClick={handlePrint}>
          Print
        </Button>
        {/* <Button>Save</Button> */}
      </Group>
      <Printable ref={componentRef}>
        <Image src="/images/scc-logo.jpg" alt="logo" height={150} width={150} />
        <Section>
          <List style={{ listStyle: "none" }}>
            <List.Item>
              Today&apos;s Date: {(values.date && dayjs(values.date).format("MM/DD/YYYY")) || "date"}
            </List.Item>
            <List.Item>Teacher: name</List.Item>
            <List.Item>
              Subject: {gradeSubject?.subject} {subject.join(",")}
            </List.Item>
            <List.Item>Grade Level: {gradeSubject?.grade}</List.Item>
          </List>
        </Section>
        <Divider />
        <Section>
          <Text style={{ textDecoration: "underline", fontWeight: "bold" }}>Opening</Text>
          <List style={{ listStyle: "none" }}>
            <List.Item>Minutes: {values.opening.time || "10"}</List.Item>
            <List.Item>Strategy: {values.opening.strategy.join(" , ") || "Strategy"}</List.Item>
            <List.Item>
              Details:
              <RichTextView value={values.opening.details} />
            </List.Item>
          </List>
        </Section>
        <Divider />
        {values?.body.map((body, index) => (
          <Section key={index}>
            <Text style={{ textDecoration: "underline", fontWeight: "bold" }}>Body</Text>
            <List style={{ listStyle: "none" }}>
              <List.Item>Minutes:{body.time}</List.Item>
              <List.Item>Material/Resources:{body.materials}</List.Item>
              <List.Item>Work Type: {body.workType}</List.Item>
              <List.Item>
                Details:
                <RichTextView value={body.workTypeDetails} />
              </List.Item>
              <List.Item>Strategy: {body.strategy.join(" , ")}</List.Item>
            </List>
          </Section>
        ))}
        <Divider />
        <Section>
          <Text style={{ textDecoration: "underline", fontWeight: "bold" }}>Closing</Text>
          <List style={{ listStyle: "none" }}>
            <List.Item>Minutes: {values.closing.time || "10"}</List.Item>
            <List.Item>Strategy: {values.closing.strategy.join(" , ") || "strategy"}</List.Item>
            <List.Item>
              Details:
              <RichTextView value={values.closing.details} />
            </List.Item>
          </List>
        </Section>
        <Divider />
        <Section>
          <Table data={data} columns={columns} />
        </Section>
      </Printable>
    </Container>
  );
};

export default LessonSummary;
