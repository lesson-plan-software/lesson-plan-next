import { DatePicker as Mantine, DatePickerProps } from "@mantine/dates";
import { CalendarIcon } from "@radix-ui/react-icons";
import { Control, Controller } from "react-hook-form";

interface Props extends DatePickerProps {
  control: Control;
  name: string;
}

const DatePicker = ({ name, control, ...rest }: Props) => {
  //todo: look for better way to do this
  const dateValue = name.split(".");
  const value = control._formValues[dateValue[0]][dateValue[1]];

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <Mantine
          {...field}
          {...rest}
          inputFormat="MM/DD/YY"
          labelFormat="MM/DD/YY"
          icon={<CalendarIcon />}
          value={value && new Date(value)}
          excludeDate={date => date.getDay() === 0 || date.getDay() === 6}
        />
      )}
    />
  );
};

export default DatePicker;
