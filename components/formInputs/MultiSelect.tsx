import type { MultiSelectProps } from "@mantine/core";
import { MultiSelect as Mantine } from "@mantine/core";
import { Control, Controller, useFormContext } from "react-hook-form";

interface Props extends MultiSelectProps {
  control: Control;
  name: string;
}

const MultiSelect = ({ name, control, ...rest }: Props) => (
  <Controller
    name={name}
    control={control}
    defaultValue={[]}
    render={({ field }) => <Mantine {...field} {...rest} />}
  />
);

export default MultiSelect;
