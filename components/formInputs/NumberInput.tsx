import type { NumberInputProps } from "@mantine/core";
import { NumberInput as Mantine } from "@mantine/core";
import { Control, Controller } from "react-hook-form";

interface Props extends NumberInputProps {
  control: Control;
  name: string;
  min?: number;
}

const NumberInput = ({ control, name, min = 0, ...rest }: Props) => (
  <Controller name={name} control={control} render={({ field }) => <Mantine min={min} {...field} {...rest} />} />
);

export default NumberInput;
