import { AppShell, Burger, Header, MediaQuery, Navbar, Text } from "@mantine/core";
import Image from "next/image";
import { useState } from "react";

import NavbarMenu from "../NavbarMenu";

const HomeNavbar = ({ children }) => {
  const [opened, setOpened] = useState(false);

  return (
    <AppShell
      // navbarOffsetBreakpoint controls when navbar should no longer be offset with padding-left
      navbarOffsetBreakpoint="sm"
      // fixed prop on AppShell will be automatically added to Header and Navbar
      fixed
      navbar={
        <Navbar
          padding="sm"
          // Breakpoint at which navbar will be hidden if hidden prop is true
          hiddenBreakpoint="lg"
          // Hides navbar when viewport size is less than value specified in hiddenBreakpoint
          hidden={!opened}
          width={{ lg: 175 }}
        >
          <NavbarMenu />
        </Navbar>
      }
      header={
        <Header height={70} padding="md">
          {/* You can handle other responsive styles with MediaQuery component or createStyles function */}
          <div
            style={{
              display: "flex",
              alignItems: "center",
              height: "100%"
            }}
          >
            <div
              style={{
                position: "relative",
                width: "65px",
                height: "65px"
              }}
            >
              <Image src="/images/scc-logo.jpg" alt="logo" layout="fill" />
            </div>
            <MediaQuery largerThan="lg" styles={{ display: "none" }}>
              <Burger opened={opened} onClick={() => setOpened(o => !o)} size="sm" mr="xl" />
            </MediaQuery>
            <Text style={{ margin: "auto 0 auto auto" }}>Hello, Name</Text>
          </div>
        </Header>
      }
    >
      {children}
    </AppShell>
  );
};

export default HomeNavbar;
