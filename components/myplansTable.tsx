import { Table } from "@mantine/core";
import { useEffect } from "react";
import { useFilters, useTable } from "react-table";

const MyPlansTable = ({ columns, data, dataSubmit }) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow, setFilter } = useTable(
    {
      columns,
      data
    },
    useFilters
  );

  useEffect(() => {
    console.log("filtered");
    // setFilter('name', dataSubmit)
  }, [dataSubmit]);

  return (
    <Table
      sx={theme => ({
        // tableLayout: "fixed",
        border: "solid black 1px",
        /* thead: {
        backgroundColor: theme.colors.gray[6],
        "tr > th": {
          color: "black",
        },
      }, */
        "tbody tr:nth-of-type(even)": {
          backgroundColor: theme.colors.brand[6],
          color: "white"
        },

        "tbody > tr > td": {
          borderBottom: "solid black 1px"
        },
        "thead > tr > th": {
          borderBottom: "none"
        }
        /* td: {
        wordWrap: "break-word",
      }, */
      })}
      {...getTableProps()}
    >
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map(row => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => (
                <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default MyPlansTable;
