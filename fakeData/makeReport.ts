import dayjs from "dayjs";

const reportData = data => ({
  standard: data.standard.standard,
  freq1: "None",
  freq2: dayjs(data.date).format("MM/DD/YY"),
  freq3: dayjs(data.date).format("MM/DD/YY"),
  freq4: "None",
  mins1: 0,
  mins2: data.time,
  mins3: data.time,
  mins4: 0
});

const makeReportData = data => {
  const makeDataLevel = data.map(el => reportData(el));

  return makeDataLevel;
};

export default makeReportData;
