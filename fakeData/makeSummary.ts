const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => ({
  standard:
    "Creatine supplementation is the reference compound for increasing muscular creatine levels; there is variability in this increase, however, with some nonresponders.",
  time: 15,
  dok: "DOK3"
});

const makeSummary = (...lens) => {
  const makeDataLevel = (depth = 0) => {
    const len = lens[depth];
    return range(len).map(d => ({
      ...newPerson()
    }));
  };

  return makeDataLevel();
};

export default makeSummary;
