import { NextApiRequest, NextApiResponse } from "next";

import { Lesson } from "../../../interfaces";
import { sampleLessonData } from "../../../utils/sample-data";

const dateFilter = (lessons: Lesson[], date: Date) => {
  const lessonForDay = lessons.filter(lesson => {
    const startDate = new Date(lesson.date.start);
    const endDate = new Date(lesson.date.end);

    return date >= startDate && date <= endDate;
  });

  return lessonForDay;
};

const handler = (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    if (_req.method === "POST") {
      const date = _req.body;

      const filtered = dateFilter(sampleLessonData, new Date(date));

      return res.status(200).json(filtered);
    }

    if (!Array.isArray(sampleLessonData)) {
      throw new Error("Cannot find user data");
    }

    res.status(200).json(sampleLessonData);
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

export default handler;
