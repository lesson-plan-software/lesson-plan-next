import { Col, Container, Grid } from "@mantine/core";
import { ReactElement } from "react";

import Chart from "../components/Chart";
import SiteLayout from "../components/layouts/SiteLayout";
import LessonView from "../components/LessonView";
import PlanActionCard from "../components/PlanActionCard";
import UnitPlanCard from "../components/UnitPlanCard";

const IndexPage = () => (
  <Container>
    <Grid>
      <Col span={12} style={{ width: "100%", height: 300 }}>
        <Chart />
      </Col>
      <Col span={8}>
        <Grid gutter="sm">
          <Col span={6}>
            <Grid>
              <Col span={12}>
                <PlanActionCard color="#002a5e" content="1" title="Lesson Plans in Progress" />
              </Col>
              {new Array(4).fill(0).map((e, i) => (
                <Col span={12} key={i}>
                  <UnitPlanCard name="name" grade={2} date={{ start: "start", end: "end" }} />
                </Col>
              ))}
            </Grid>
          </Col>
          <Col span={6}>
            <Grid>
              <Col span={12}>
                <PlanActionCard color="purple" content="5" title="Plans to Rate" />
              </Col>
              {new Array(4).fill(0).map((e, i) => (
                <Col span={12} key={i}>
                  <UnitPlanCard name="name" grade={2} date={{ start: "start", end: "end" }} />
                </Col>
              ))}
            </Grid>
          </Col>
        </Grid>
      </Col>
      <Col span={4}>
        <LessonView />
      </Col>
    </Grid>
  </Container>
);

IndexPage.getLayout = (page: ReactElement) => <SiteLayout head="Home Page">{page}</SiteLayout>;

export default IndexPage;
