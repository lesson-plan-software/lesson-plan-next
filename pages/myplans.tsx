import { Button, Center, Col, Container, Divider, Grid, Group, Text } from "@mantine/core";
import React, { ReactElement, useState } from "react";
import { FormProvider, NestedValue, useForm, useWatch } from "react-hook-form";

import { DatePicker, MultiSelect, Select } from "../components/formInputs";
import SiteLayout from "../components/layouts/SiteLayout";
import MyplansTable from "../components/myplansTable";

const dropdowntest = {
  value: "1",
  label: "1"
};

interface planFilterProps {
  date: NestedValue<{
    start: Date | string;
    end: Date | string;
  }>;
  title: Array<string>;
  rating: Array<string>;
  favorites: Array<string>;
  subjects: Array<string>;
}

interface standardFilterProps {
  grade: string;
  subjects: Array<string>;
  contentArea: string;
  standards: Array<string>;
}

const MyPlans = () => {
  const [submitted, setSubmitted] = useState({});
  const form = useForm<planFilterProps>({
    defaultValues: {
      date: { start: "", end: "" },
      title: [],
      rating: [],
      favorites: [],
      subjects: []
    }
  });
  const form2 = useForm<standardFilterProps>({
    defaultValues: {
      grade: "",
      subjects: [],
      contentArea: "",
      standards: []
    }
  });
  const { control, handleSubmit } = form;
  const { control: control2, handleSubmit: handleSubmit2 } = form2;

  const standardFields = useWatch({
    control: control2,
    name: ["grade", "subjects", "contentArea"]
  });
  const columns = React.useMemo(
    () => [
      {
        Header: "Plan Date(s)",
        accessor: "planDate"
      },
      {
        Header: "Title",
        accessor: "title"
      },
      {
        Header: "Rating",
        accessor: "rating"
      }
    ],
    []
  );

  // const data = React.useMemo(() => makeData(3), [])
  const data = React.useMemo(
    () => [
      { planDate: "10/1", title: "Egyptian", rating: 3 },
      { planDate: "10/1", title: "Fractions", rating: 1 },
      { planDate: "10/8", title: "Algerbra", rating: 2 },
      { planDate: "10/9", title: "Europe", rating: 1 }
    ],
    []
  );

  const onSubmit = async formData => {
    setSubmitted(formData);
  };
  return (
    <div>
      <FormProvider {...form}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid my={15}>
            <Col span={2}>
              <DatePicker control={control} name="date.start" label="Start Date" placeholder="Start Date" />
            </Col>
            <Col span={2}>
              <DatePicker control={control} name="date.end" label="End Date" placeholder="End Date" />
            </Col>
            <Col span={2}>
              <MultiSelect data={[dropdowntest]} name="title" control={control} placeholder="Title" label="Title" />
            </Col>
            <Col span={2}>
              <MultiSelect data={[dropdowntest]} name="rating" control={control} placeholder="Rating" label="Rating" />
            </Col>
            <Col span={2}>
              <MultiSelect
                data={[dropdowntest]}
                name="favorites"
                control={control}
                placeholder="Favorites"
                label="Favorites"
              />
            </Col>
            <Col span={2}>
              <MultiSelect
                data={[dropdowntest]}
                name="subjects"
                control={control}
                placeholder="Subject"
                label="Subject"
              />
            </Col>
          </Grid>
          <Center my={15}>
            <Button type="submit">Search</Button>
          </Center>
        </form>
      </FormProvider>
      <Divider />
      <FormProvider {...form2}>
        <form onSubmit={handleSubmit2(onSubmit)}>
          <Grid my={15} justify="center">
            <Col
              span={2}
              style={{
                display: "flex",
                alignItems: "center"
              }}
            >
              <Text>Search by Standard:</Text>
            </Col>
            <Col span={2}>
              <Select data={[dropdowntest]} name="grade" control={control2} placeholder="Grade(s)" label="Grade(s)" />
            </Col>
            <Col span={2}>
              {standardFields[0] && (
                <MultiSelect
                  data={[dropdowntest]}
                  name="subjects"
                  control={control2}
                  placeholder="Subject(s)"
                  label="Subject(s)"
                />
              )}
            </Col>
            <Col span={2}>
              {standardFields[1]?.length > 0 && (
                <Select
                  data={[dropdowntest]}
                  name="contentArea"
                  control={control2}
                  placeholder="Content Area"
                  label="Content Area"
                />
              )}
            </Col>
            <Col span={2}>
              {standardFields[2] && (
                <MultiSelect
                  data={[dropdowntest]}
                  name="standards"
                  control={control2}
                  placeholder="Standard(s)"
                  label="Standard(s)"
                />
              )}
            </Col>
          </Grid>
          <Center my={15}>
            <Button type="submit">Search</Button>
          </Center>
        </form>
      </FormProvider>
      <Divider />
      <Group position="center" style={{ margin: "15px 0" }}>
        <Button size="xl">Social Studies Plans</Button>
        <Button size="xl">Math Plans</Button>
        <Button size="xl">Art Plans</Button>
      </Group>
      <div>
        <Container>
          <Text my={15}>Search Results</Text>
          <MyplansTable columns={columns} data={data} dataSubmit={submitted} />
        </Container>
      </div>
    </div>
  );
};

MyPlans.getLayout = (page: ReactElement) => <SiteLayout head="My Plans">{page}</SiteLayout>;

export default MyPlans;
