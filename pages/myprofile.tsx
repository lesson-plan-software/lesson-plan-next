import { ActionIcon, Button, Container, Modal } from "@mantine/core";
import { PlusIcon } from "@radix-ui/react-icons";
import React, { ReactElement, useEffect, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { useQuery } from "react-query";

import { NumberInput, Select } from "../components/formInputs";
import SiteLayout from "../components/layouts/SiteLayout";
import Table from "../components/StyledTable";
import grades from "../constants/grades";
import subjects from "../constants/subjects";

const defaultValues = {
  period: "",
  grade: "",
  subject: "",
  time: 20,
  open_default: 10,
  close_default: 10
};

const fetcher = () => fetch("/api/periods").then(res => res.json());

const MyProfile = () => {
  const [opened, setOpened] = useState(false);

  const form = useForm({ defaultValues });
  const { control, handleSubmit, reset } = form;

  const [profiles, setProfiles] = useState(null);

  const { isLoading, isError, isFetching, data } = useQuery("userPeriods", fetcher);

  const columns = React.useMemo(
    () => [
      {
        Header: "Period",
        accessor: "period"
      },
      {
        Header: "Grade",
        accessor: "grade"
      },
      {
        Header: "Subject",
        accessor: "subject"
      },
      {
        Header: "Time",
        accessor: "time"
      },
      {
        Header: "Open default",
        accessor: "open_default"
      },
      {
        Header: "Close default",
        accessor: "close_default"
      }
    ],
    []
  );

  useEffect(() => {
    if (data) {
      setProfiles(data);
    }
  }, [data]);

  const onSubmit = values => {
    setProfiles(old => [
      ...old,
      {
        ...values,
        period: profiles.length + 1
      }
    ]);
    setOpened(false);
    reset(defaultValues);
  };

  if (isLoading || profiles === null) return "Loading...";

  if (isError) return "An error has occurred: ";

  return (
    <div>
      <Container>
        <FormProvider {...form}>
          <Modal opened={opened} onClose={() => setOpened(false)} title="Create New Period" closeOnClickOutside={false}>
            <form>
              <Select control={control} label="Grade" placeholder="K" name="grade" data={grades} required />
              <Select control={control} label="Subject" placeholder="Math" name="subject" data={subjects} required />
              <NumberInput
                control={control}
                label="Time"
                placeholder="placeholder"
                name="time"
                defaultValue={10}
                min={10}
                required
              />
              <NumberInput
                control={control}
                label="Default Opening Time"
                placeholder="placeholder"
                name="open_default"
                defaultValue={5}
              />
              <NumberInput
                control={control}
                label="Default Closing Time"
                placeholder="placeholder"
                name="close_default"
                defaultValue={5}
              />
              <Button mt={10} onClick={handleSubmit(onSubmit)}>
                Submit
              </Button>
            </form>
          </Modal>
        </FormProvider>
        <Table data={profiles} columns={columns} />

        <ActionIcon onClick={() => setOpened(true)}>
          <PlusIcon width={28} height={28} />
        </ActionIcon>
      </Container>
    </div>
  );
};

MyProfile.getLayout = (page: ReactElement) => <SiteLayout head="Profile">{page}</SiteLayout>;

export default MyProfile;
