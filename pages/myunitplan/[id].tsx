import { useRouter } from "next/router";
import { ReactElement } from "react";

import SiteLayout from "../../components/layouts/SiteLayout";

const Plan = () => {
  const router = useRouter();
  const { id } = router.query;

  return <p>Post: {id}</p>;
};

Plan.getLayout = (page: ReactElement) => <SiteLayout head="Home Page">{page}</SiteLayout>;

export default Plan;
