import { useRouter } from "next/router";
import React, { ReactElement, useState } from "react";
import { useQuery } from "react-query";

import SiteLayout from "../../components/layouts/SiteLayout";
import LessonPlanFormWizard from "../../components/LessonPlanFormWizard";

const defaultValues = {
  period: [],
  planName: "ID1",
  date: {
    start: null,
    end: null
  },
  subjects: [],
  careers: [],
  standardsChosen: [],
  lessons: [],
  finished: false
};

const fetcher = () => fetch("/api/lessonPlans/0").then(res => res.json());

const Create = () => {
  const router = useRouter();
  const { id } = router.query;
  const newLesson = router.query.new;

  const {
    isLoading: isLoadingQuery,
    isError,
    data
  } = useQuery(["lessonPlan"], fetcher, {
    refetchInterval: false,
    refetchIntervalInBackground: false
  });

  const [isLoading, setIsLoading] = useState(false);
  const [formFill, setFormFill] = useState(defaultValues);

  return !isLoadingQuery ? <LessonPlanFormWizard defaultValues={formFill} /> : null;
};

Create.getLayout = (page: ReactElement) => <SiteLayout head="Create New Plan">{page}</SiteLayout>;

export default Create;
