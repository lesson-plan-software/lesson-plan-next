import { Button, Group } from "@mantine/core";
import { DatePicker } from "@mantine/dates";
import { CalendarIcon } from "@radix-ui/react-icons";
import React, { ReactElement, useState } from "react";
import { useQuery } from "react-query";
import { Printable, usePrint } from "utils/printAndSave";

import SiteLayout from "../components/layouts/SiteLayout";
import StyledTable from "../components/StyledTable";
import makeReportData from "../fakeData/makeReport";

const fetcher = () => fetch("/api/report").then(res => res.json());

const Reports = () => {
  const { isLoading, data } = useQuery("report", fetcher);
  const { componentRef, handlePrint } = usePrint();
  const [startDate, setStartDate] = useState(new Date(new Date().getFullYear(), 7));
  const [endDate, setEndDate] = useState(new Date());

  const columns = React.useMemo(
    () => [
      {
        Header: " ",
        columns: [
          {
            Header: "Standard",
            accessor: "standard"
          }
        ]
      },
      {
        Header: "Frequency & last lesson",
        columns: [
          {
            Header: "1",
            accessor: "freq1"
          },
          {
            Header: "2",
            accessor: "freq2"
          },
          {
            Header: "3",
            accessor: "freq3"
          },
          {
            Header: "4",
            accessor: "freq4"
          }
        ]
      },
      {
        Header: "Minutes per level",
        columns: [
          {
            Header: "1",
            accessor: "mins1"
          },
          {
            Header: "2",
            accessor: "mins2"
          },
          {
            Header: "3",
            accessor: "mins3"
          },
          {
            Header: "4",
            accessor: "mins4"
          }
        ]
      }
    ],
    []
  );
  const tableData = React.useMemo(() => {
    if (data) {
      return makeReportData(data);
    }
    return [];
  }, [data]);

  if (isLoading) return "Loading ...";

  return (
    <div style={{ paddingRight: "16px" }}>
      <Group position="apart">
        <Group>
          <DatePicker
            variant="unstyled"
            defaultValue={new Date(new Date().getFullYear(), 7)}
            inputFormat="MM/DD/YYYY"
            labelFormat="MM/DD/YYYY"
            minDate={new Date(new Date().getFullYear(), 7)}
            icon={<CalendarIcon />}
            clearable={false}
            disabled
          />
          <DatePicker
            variant="unstyled"
            defaultValue={new Date()}
            inputFormat="MM/DD/YYYY"
            labelFormat="MM/DD/YYYY"
            icon={<CalendarIcon />}
            placeholder="Select Date"
          />
        </Group>
        <Group>
          <Button onClick={handlePrint}>Print</Button>
          {/* <Button>Save</Button> */}
        </Group>
      </Group>
      <div style={{ marginTop: "16px" }}>
        <Printable ref={componentRef}>
          <StyledTable columns={columns} data={tableData} />
        </Printable>
      </div>
    </div>
  );
};

Reports.getLayout = (page: ReactElement) => <SiteLayout head="Reports">{page}</SiteLayout>;

export default Reports;
