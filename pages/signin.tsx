import NoAuthLayout from "@components/layouts/NoAuthLayout";
import { PasswordInput, TextInput } from "@mantine/core";
import React, { ReactElement } from "react";

const SignIn = () => (
  <div>
    <TextInput />
    <PasswordInput />
  </div>
);

SignIn.getLayout = (page: ReactElement) => <NoAuthLayout head="Sign In">{page}</NoAuthLayout>;

export default SignIn;
