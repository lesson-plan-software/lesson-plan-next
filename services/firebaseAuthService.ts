import { Auth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from "firebase/auth";

import { auth } from "../firebase";

class FirebaseAuthenticationService {
  auth: Auth;

  constructor() {
    this.auth = auth;
  }

  signUpUser = async (email: string, password: string) => createUserWithEmailAndPassword(this.auth, email, password);

  signInUser = async (email: string, password: string) => signInWithEmailAndPassword(this.auth, email, password);

  logOutUser = async () => signOut(this.auth);
}

const AuthService = new FirebaseAuthenticationService();

export default AuthService;
