import {
  addDoc,
  collection,
  CollectionReference,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  updateDoc
} from "firebase/firestore";

import { db } from "../firebase";

export default class FirebaseService {
  collectionRef: CollectionReference;

  constructor(collectionName: string) {
    this.collectionRef = collection(db, collectionName);
  }

  // get one document from collection
  findOneById = async (id: string) => {
    const documentRef = doc(this.collectionRef, id);
    const snapshot = await getDoc(documentRef);

    if (snapshot.exists()) {
      return snapshot.data();
    }

    return null;
  };

  // get multiple documents from collection
  findAll = async () => {
    const snapshot = await getDocs(this.collectionRef);

    const data = snapshot.docs.map(doc => doc.data());

    return data;
  };

  // create a document
  create = async data => {
    await addDoc(this.collectionRef, data);
  };

  // create
  update = async (id: string, data) => {
    const documentRef = doc(this.collectionRef, id);

    return updateDoc(documentRef, data);
  };

  remove = async (id: string) => {
    const documentRef = doc(this.collectionRef, id);

    return deleteDoc(documentRef);
  };
}
