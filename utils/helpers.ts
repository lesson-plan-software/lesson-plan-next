import dayjs from "dayjs";
import { Grades } from "enums";
import { Period } from "interfaces";

export const formattedDate = (date: Date | string) => dayjs(date).format("MM/DD/YY");

export const getDatesBetweenDates = (startDate: Date | string, endDate: Date | string) => {
  const startDateZero = dayjs(startDate).startOf("day");
  const endDateZero = dayjs(endDate).startOf("day");
  let dates: Date[] = [];

  let theDate = dayjs(startDateZero);

  while (theDate <= endDateZero) {
    const newDate = dayjs(theDate);
    if (newDate.get("day") !== 6 && newDate.get("day") !== 0) {
      dates = [...dates, dayjs(theDate).toDate()];
    }

    theDate = newDate.add(1, "day");
  }
  return dates;
};

export const shortestTime = (period: Period[], openOrClose: "open" | "close") => {
  const shortestN = period.reduce((prev, curr) => {
    const currOpen = curr[`${openOrClose}_default`];
    const prevOpen = prev[`${openOrClose}_default`];

    return currOpen < prevOpen ? curr : prev;
  }, period[0]);

  return shortestN[`${openOrClose}_default`];
};

export const numberOrdinal = (n: Grades) => {
  if (n === Grades.KINDERGARTEN) return n;
  const nInt = parseInt(n, 10);
  const s = ["th", "st", "nd", "rd"];
  const v = nInt % 100;
  return `${nInt + (s[(v - 20) % 10] || s[v] || s[0])} grade`;
};

// const getDatesBetweenDates = (startDate, endDate) => {
//   const startDateZero = new Date(new Date(startDate).setHours(0, 0, 0, 0));
//   const endDateZero = new Date(new Date(endDate).setHours(0, 0, 0, 0));
//   let dates = [];
//   const theDate = new Date(startDateZero);

//   while (theDate <= endDateZero) {
//     const newDate = new Date(theDate);
//     if (newDate.getDay() !== 6 && newDate.getDay() !== 0) {
//       dates = [...dates, new Date(theDate)];
//     }
//     theDate.setDate(theDate.getDate() + 1);
//   }
//   return dates;
// };
