import React, { useRef } from "react";
import { useReactToPrint } from "react-to-print";

const usePrint = () => {
  const componentRef = useRef(null);

  const handlePrint = useReactToPrint({
    content: () => componentRef.current
  });

  return { componentRef, handlePrint };
};

interface Props {
  children: React.ReactNode;
}

const Printable = React.forwardRef(({ children }: Props, ref) => <div ref={ref}>{children}</div>);

export { Printable, usePrint };
